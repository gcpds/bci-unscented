from eeg_framework import EEGFramework
import sys
import os

from PySide2.QtWidgets import QApplication
from pyside_material import apply_stylesheet, set_icons_theme


from multiprocessing import freeze_support

if __name__ == "__main__":
    freeze_support()

    app = QApplication(sys.argv)
    app.setStyle("Fusion")

    # apply_stylesheet(app, theme='light_blue.xml', light_secondary=True)
    apply_stylesheet(app, theme='dark_cyan.xml')
    # # set_icons_theme(theme='dark_cyan.xml', resource='bci_unscented/qtgui/icons/resource_rc.py')

    frame = EEGFramework()

    frame.main.show()
    app.exec_()

