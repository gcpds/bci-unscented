# -*- coding: utf-8 -*-

# Resource object code
#
# Created: Sun Aug 11 20:12:10 2019
#      by: The Resource Compiler for PySide2 (Qt v5.13.0)
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore

qt_resource_data = b"\
\x00\x00(\xc6\
<\
?xml version=\x221.\
0\x22 encoding=\x22UTF\
-8\x22 standalone=\x22\
no\x22?>\x0a<!-- Creat\
ed with Inkscape\
 (http://www.ink\
scape.org/) -->\x0a\
\x0a<svg\x0a   xmlns:d\
c=\x22http://purl.o\
rg/dc/elements/1\
.1/\x22\x0a   xmlns:cc\
=\x22http://creativ\
ecommons.org/ns#\
\x22\x0a   xmlns:rdf=\x22\
http://www.w3.or\
g/1999/02/22-rdf\
-syntax-ns#\x22\x0a   \
xmlns:svg=\x22http:\
//www.w3.org/200\
0/svg\x22\x0a   xmlns=\
\x22http://www.w3.o\
rg/2000/svg\x22\x0a   \
xmlns:sodipodi=\x22\
http://sodipodi.\
sourceforge.net/\
DTD/sodipodi-0.d\
td\x22\x0a   xmlns:ink\
scape=\x22http://ww\
w.inkscape.org/n\
amespaces/inksca\
pe\x22\x0a   width=\x2251\
2\x22\x0a   height=\x2251\
2\x22\x0a   viewBox=\x220\
 0 135.46667 135\
.46667\x22\x0a   versi\
on=\x221.1\x22\x0a   id=\x22\
svg8\x22\x0a   inkscap\
e:version=\x220.92.\
4 5da689c313, 20\
19-01-14\x22\x0a   sod\
ipodi:docname=\x22u\
nscented-def3.sv\
g\x22>\x0a  <defs\x0a    \
 id=\x22defs2\x22 />\x0a \
 <sodipodi:named\
view\x0a     id=\x22ba\
se\x22\x0a     pagecol\
or=\x22#ffffff\x22\x0a   \
  bordercolor=\x22#\
666666\x22\x0a     bor\
deropacity=\x221.0\x22\
\x0a     inkscape:p\
ageopacity=\x220.0\x22\
\x0a     inkscape:p\
ageshadow=\x222\x22\x0a  \
   inkscape:zoom\
=\x221\x22\x0a     inksca\
pe:cx=\x22428.92298\
\x22\x0a     inkscape:\
cy=\x22223.78954\x22\x0a \
    inkscape:doc\
ument-units=\x22px\x22\
\x0a     inkscape:c\
urrent-layer=\x22la\
yer1\x22\x0a     showg\
rid=\x22false\x22\x0a    \
 inkscape:window\
-width=\x221920\x22\x0a  \
   inkscape:wind\
ow-height=\x221015\x22\
\x0a     inkscape:w\
indow-x=\x220\x22\x0a    \
 inkscape:window\
-y=\x220\x22\x0a     inks\
cape:window-maxi\
mized=\x221\x22\x0a     u\
nits=\x22px\x22\x0a     i\
nkscape:showpage\
shadow=\x22false\x22\x0a \
    showguides=\x22\
false\x22\x0a     inks\
cape:snap-bbox=\x22\
true\x22\x0a     inksc\
ape:bbox-paths=\x22\
true\x22\x0a     inksc\
ape:bbox-nodes=\x22\
true\x22\x0a     inksc\
ape:snap-bbox-mi\
dpoints=\x22true\x22\x0a \
    inkscape:sna\
p-bbox-edge-midp\
oints=\x22true\x22\x0a   \
  inkscape:objec\
t-paths=\x22true\x22\x0a \
    inkscape:sna\
p-intersection-p\
aths=\x22true\x22\x0a    \
 inkscape:snap-m\
idpoints=\x22true\x22\x0a\
     inkscape:sn\
ap-smooth-nodes=\
\x22true\x22\x0a     inks\
cape:snap-object\
-midpoints=\x22true\
\x22\x0a     inkscape:\
snap-center=\x22tru\
e\x22\x0a     fit-marg\
in-top=\x220\x22\x0a     \
fit-margin-left=\
\x220\x22\x0a     fit-mar\
gin-right=\x220\x22\x0a  \
   fit-margin-bo\
ttom=\x220\x22 />\x0a  <m\
etadata\x0a     id=\
\x22metadata5\x22>\x0a   \
 <rdf:RDF>\x0a     \
 <cc:Work\x0a      \
   rdf:about=\x22\x22>\
\x0a        <dc:for\
mat>image/svg+xm\
l</dc:format>\x0a  \
      <dc:type\x0a \
          rdf:re\
source=\x22http://p\
url.org/dc/dcmit\
ype/StillImage\x22 \
/>\x0a        <dc:t\
itle></dc:title>\
\x0a      </cc:Work\
>\x0a    </rdf:RDF>\
\x0a  </metadata>\x0a \
 <g\x0a     inkscap\
e:label=\x22Capa 1\x22\
\x0a     inkscape:g\
roupmode=\x22layer\x22\
\x0a     id=\x22layer1\
\x22\x0a     transform\
=\x22translate(21.7\
00237,-922.51954\
)\x22>\x0a    <path\x0a  \
     sodipodi:ty\
pe=\x22star\x22\x0a      \
 style=\x22opacity:\
1;fill:#f2f2f2;f\
ill-opacity:1;st\
roke:none;stroke\
-width:1.1627526\
3;stroke-linecap\
:round;stroke-li\
nejoin:round;str\
oke-miterlimit:4\
;stroke-dasharra\
y:none;stroke-da\
shoffset:0;strok\
e-opacity:0.1415\
525\x22\x0a       id=\x22\
path1578\x22\x0a      \
 sodipodi:sides=\
\x224\x22\x0a       sodip\
odi:cx=\x2246.03309\
6\x22\x0a       sodipo\
di:cy=\x22990.25287\
\x22\x0a       sodipod\
i:r1=\x2275.712967\x22\
\x0a       sodipodi\
:r2=\x2253.537148\x22\x0a\
       sodipodi:\
arg1=\x220.78539816\
\x22\x0a       sodipod\
i:arg2=\x221.570796\
3\x22\x0a       inksca\
pe:flatsided=\x22tr\
ue\x22\x0a       inksc\
ape:rounded=\x220.2\
5\x22\x0a       inksca\
pe:randomized=\x220\
\x22\x0a       d=\x22m 99\
.570249,1043.79 \
c -18.928242,18.\
9283 -88.146063,\
18.9283 -107.074\
3048,0 -18.92824\
22,-18.9282 -18.\
9282422,-88.1460\
4 -4e-7,-107.074\
28 18.9282412,-1\
8.92825 88.14606\
32,-18.92825 107\
.0743042,0 18.92\
8242,18.92824 18\
.928242,88.14608\
 10e-7,107.07428\
 z\x22\x0a       inksc\
ape:transform-ce\
nter-y=\x22-2.04209\
79e-05\x22 />\x0a    <\
circle\x0a       cy\
=\x22139.7252\x22\x0a    \
   cx=\x22-591.7011\
1\x22\x0a       id=\x22ci\
rcle1545\x22\x0a      \
 style=\x22opacity:\
0.67299996;fill:\
#aaaaff;fill-opa\
city:0.30136988;\
stroke:#000000;s\
troke-width:0.30\
404934;stroke-mi\
terlimit:4;strok\
e-dasharray:none\
;stroke-opacity:\
1\x22\x0a       r=\x220\x22 \
/>\x0a    <circle\x0a \
      r=\x220\x22\x0a    \
   style=\x22opacit\
y:0.67299996;fil\
l:#aaaaff;fill-o\
pacity:0.3013698\
8;stroke:#000000\
;stroke-width:0.\
30404934;stroke-\
miterlimit:4;str\
oke-dasharray:no\
ne;stroke-opacit\
y:1\x22\x0a       id=\x22\
ellipse1617\x22\x0a   \
    cx=\x22-993.512\
82\x22\x0a       cy=\x221\
68.13614\x22 />\x0a   \
 <circle\x0a       \
r=\x220\x22\x0a       cy=\
\x22733.2077\x22\x0a     \
  cx=\x22-958.14404\
\x22\x0a       id=\x22ell\
ipse1906\x22\x0a      \
 style=\x22opacity:\
0.67299996;fill:\
none;fill-opacit\
y:0.30136988;str\
oke:#000000;stro\
ke-width:0.79374\
999;stroke-miter\
limit:4;stroke-d\
asharray:none;st\
roke-opacity:1\x22 \
/>\x0a    <circle\x0a \
      r=\x220\x22\x0a    \
   cy=\x22582.33099\
\x22\x0a       cx=\x22-15\
00.9722\x22\x0a       \
id=\x22ellipse1926\x22\
\x0a       style=\x22o\
pacity:0.6729999\
6;fill:none;fill\
-opacity:0.30136\
988;stroke:#0000\
00;stroke-width:\
0.79374999;strok\
e-miterlimit:4;s\
troke-dasharray:\
none;stroke-opac\
ity:1\x22 />\x0a    <c\
ircle\x0a       r=\x22\
0\x22\x0a       cy=\x2225\
5.9781\x22\x0a       c\
x=\x22-958.14404\x22\x0a \
      id=\x22ellips\
e1946\x22\x0a       st\
yle=\x22opacity:0.6\
7299996;fill:non\
e;fill-opacity:0\
.30136988;stroke\
:#000000;stroke-\
width:0.79374999\
;stroke-miterlim\
it:4;stroke-dash\
array:none;strok\
e-opacity:1\x22 />\x0a\
    <g\x0a       id\
=\x22g1555\x22\x0a       \
transform=\x22matri\
x(0.60155603,0,0\
,0.60155603,-89.\
500471,362.97673\
)\x22>\x0a      <path\x0a\
         style=\x22\
opacity:0.75;fil\
l:#ffffff;fill-o\
pacity:1;stroke:\
#d7e3f4;stroke-w\
idth:5.29166651;\
stroke-miterlimi\
t:4;stroke-dasha\
rray:none;stroke\
-dashoffset:0;st\
roke-opacity:1\x22\x0a\
         d=\x22m 23\
4.01891,962.9846\
1 a 39.427414,39\
.427414 0 0 0 -2\
4.82845,8.82375 \
39.427414,39.427\
414 0 0 0 -16.57\
729,-3.70366 39.\
427414,39.427414\
 0 0 0 -32.67552\
,17.38397 39.427\
414,39.427414 0 \
0 0 -31.75466,38\
.63583 39.427414\
,39.427414 0 0 0\
 39.42758,39.427\
6 39.427414,39.4\
27414 0 0 0 10.9\
585,-1.5741 36.7\
49241,36.749241 \
0 0 0 34.72863,2\
4.8678 36.749241\
,36.749241 0 0 0\
 3.82871,-0.2419\
 18.328236,18.32\
8236 0 0 0 18.32\
808,18.3286 18.3\
28236,18.328236 \
0 0 0 11.17399,-\
3.8148 39.427414\
,39.427414 0 0 0\
 21.87567,6.6498\
 39.427414,39.42\
7414 0 0 0 38.86\
689,-33.0874 39.\
427414,39.427414\
 0 0 0 15.05592,\
-30.9826 39.4274\
14,39.427414 0 0\
 0 -12.53928,-28\
.7693 39.427414,\
39.427414 0 0 0 \
-39.34593,-37.11\
248 39.427414,39\
.427414 0 0 0 -5\
.39967,0.41548 3\
9.427414,39.4274\
14 0 0 0 -31.123\
17,-15.25178 z m\
 8.20467,145.870\
19 a 6.8397582,6\
.8397582 0 0 0 -\
6.83937,6.8393 6\
.8397582,6.83975\
82 0 0 0 6.83937\
,6.84 6.8397582,\
6.8397582 0 0 0 \
6.83989,-6.8399 \
6.8397582,6.8397\
582 0 0 0 -6.839\
89,-6.8394 z\x22\x0a  \
       id=\x22path1\
507\x22\x0a         in\
kscape:connector\
-curvature=\x220\x22 /\
>\x0a      <circle\x0a\
         r=\x2239.4\
27414\x22\x0a         \
style=\x22opacity:0\
.75;fill:#4dd0e1\
;fill-opacity:1;\
stroke:none;stro\
ke-width:0.26458\
332;stroke-miter\
limit:4;stroke-d\
asharray:none;st\
roke-dashoffset:\
0;stroke-opacity\
:1\x22\x0a         id=\
\x22circle1509\x22\x0a   \
      cx=\x22167.61\
058\x22\x0a         cy\
=\x221024.1246\x22 />\x0a\
      <circle\x0a  \
       r=\x2239.427\
414\x22\x0a         cy\
=\x221068.3453\x22\x0a   \
      cx=\x22268.50\
4\x22\x0a         id=\x22\
circle1511\x22\x0a    \
     style=\x22opac\
ity:0.75;fill:#4\
dd0e1;fill-opaci\
ty:1;stroke:none\
;stroke-width:0.\
26458332;stroke-\
miterlimit:4;str\
oke-dasharray:no\
ne;stroke-dashof\
fset:0;stroke-op\
acity:1\x22 />\x0a    \
  <circle\x0a      \
   r=\x2236.749241\x22\
\x0a         style=\
\x22opacity:0.75;fi\
ll:#4dd0e1;fill-\
opacity:1;stroke\
:none;stroke-wid\
th:0.26458332;st\
roke-miterlimit:\
4;stroke-dasharr\
ay:none;stroke-d\
ashoffset:0;stro\
ke-opacity:1\x22\x0a  \
       id=\x22circl\
e1513\x22\x0a         \
cx=\x22213.29759\x22\x0a \
        cy=\x221050\
.0966\x22 />\x0a      \
<circle\x0a        \
 r=\x2239.427414\x22\x0a \
        cy=\x221043\
.7031\x22\x0a         \
cx=\x22282.99936\x22\x0a \
        id=\x22circ\
le1515\x22\x0a        \
 style=\x22opacity:\
0.75;fill:#4dd0e\
1;fill-opacity:1\
;stroke:none;str\
oke-width:0.2645\
8332;stroke-mite\
rlimit:4;stroke-\
dasharray:none;s\
troke-dashoffset\
:0;stroke-opacit\
y:1\x22 />\x0a      <c\
ircle\x0a         s\
tyle=\x22opacity:0.\
75;fill:#4dd0e1;\
fill-opacity:1;s\
troke:none;strok\
e-width:0.264583\
32;stroke-miterl\
imit:4;stroke-da\
sharray:none;str\
oke-dashoffset:0\
;stroke-opacity:\
1\x22\x0a         id=\x22\
circle1517\x22\x0a    \
     cx=\x22270.541\
72\x22\x0a         cy=\
\x221017.2484\x22\x0a    \
     r=\x2239.42741\
4\x22 />\x0a      <cir\
cle\x0a         r=\x22\
39.427414\x22\x0a     \
    cy=\x221007.532\
3\x22\x0a         cx=\x22\
192.61308\x22\x0a     \
    id=\x22circle15\
19\x22\x0a         sty\
le=\x22opacity:0.75\
;fill:#4dd0e1;fi\
ll-opacity:1;str\
oke:none;stroke-\
width:0.26458332\
;stroke-miterlim\
it:4;stroke-dash\
array:none;strok\
e-dashoffset:0;s\
troke-opacity:1\x22\
 />\x0a      <circl\
e\x0a         style\
=\x22opacity:0.75;f\
ill:#4dd0e1;fill\
-opacity:1;strok\
e:none;stroke-wi\
dth:0.26458332;s\
troke-miterlimit\
:4;stroke-dashar\
ray:none;stroke-\
dashoffset:0;str\
oke-opacity:1\x22\x0a \
        id=\x22circ\
le1521\x22\x0a        \
 cx=\x22234.01903\x22\x0a\
         cy=\x22100\
2.4123\x22\x0a        \
 r=\x2239.427414\x22 /\
>\x0a      <circle\x0a\
         r=\x2218.3\
28236\x22\x0a         \
style=\x22opacity:0\
.75;fill:#4dd0e1\
;fill-opacity:1;\
stroke:none;stro\
ke-width:0.26458\
332;stroke-miter\
limit:4;stroke-d\
asharray:none;st\
roke-dashoffset:\
0;stroke-opacity\
:1\x22\x0a         id=\
\x22circle1523\x22\x0a   \
      cx=\x22235.45\
457\x22\x0a         cy\
=\x221086.6095\x22 />\x0a\
      <circle\x0a  \
       r=\x226.8397\
584\x22\x0a         st\
yle=\x22opacity:0.7\
5;fill:#4dd0e1;f\
ill-opacity:1;st\
roke:none;stroke\
-width:0.2645833\
2;stroke-miterli\
mit:4;stroke-das\
harray:none;stro\
ke-dashoffset:0;\
stroke-opacity:1\
\x22\x0a         id=\x22c\
ircle1525\x22\x0a     \
    cx=\x22242.2237\
4\x22\x0a         cy=\x22\
1115.6946\x22 />\x0a  \
    <path\x0a      \
   id=\x22path1527\x22\
\x0a         style=\
\x22opacity:1;fill:\
none;fill-opacit\
y:0.6255708;stro\
ke:#ffffff;strok\
e-width:0.529166\
7;stroke-miterli\
mit:4;stroke-das\
harray:none;stro\
ke-dashoffset:0;\
stroke-opacity:1\
\x22\x0a         d=\x22m \
249.06349,1115.6\
946 a 6.8397584,\
6.8397584 0 0 1 \
-6.83976,6.8397 \
6.8397584,6.8397\
584 0 0 1 -6.839\
76,-6.8397 6.839\
7584,6.8397584 0\
 0 1 6.83976,-6.\
8398 6.8397584,6\
.8397584 0 0 1 6\
.83976,6.8398 z \
m 4.71931,-29.08\
51 a 18.328236,1\
8.328236 0 0 1 -\
18.32823,18.3282\
 18.328236,18.32\
8236 0 0 1 -18.3\
2824,-18.3282 18\
.328236,18.32823\
6 0 0 1 18.32824\
,-18.3282 18.328\
236,18.328236 0 \
0 1 18.32823,18.\
3282 z m 19.6636\
4,-84.1972 a 39.\
427414,39.427414\
 0 0 1 -39.42742\
,39.4274 39.4274\
14,39.427414 0 0\
 1 -39.42741,-39\
.4274 39.427414,\
39.427414 0 0 1 \
39.42741,-39.427\
43 39.427414,39.\
427414 0 0 1 39.\
42742,39.42743 z\
 m -41.40595,5.1\
2 a 39.427414,39\
.427414 0 0 1 -3\
9.42741,39.4275 \
39.427414,39.427\
414 0 0 1 -39.42\
742,-39.4275 39.\
427414,39.427414\
 0 0 1 39.42742,\
-39.42737 39.427\
414,39.427414 0 \
0 1 39.42741,39.\
42737 z m 77.928\
66,9.7161 a 39.4\
27414,39.427414 \
0 0 1 -39.42742,\
39.4274 39.42741\
4,39.427414 0 0 \
1 -39.42742,-39.\
4274 39.427414,3\
9.427414 0 0 1 3\
9.42742,-39.4274\
1 39.427414,39.4\
27414 0 0 1 39.4\
2742,39.42741 z \
m 12.45764,26.45\
48 a 39.427414,3\
9.427414 0 0 1 -\
39.42742,39.4274\
 39.427414,39.42\
7414 0 0 1 -39.4\
2742,-39.4274 39\
.427414,39.42741\
4 0 0 1 39.42742\
,-39.4274 39.427\
414,39.427414 0 \
0 1 39.42742,39.\
4274 z m -72.379\
96,6.3934 a 36.7\
49241,36.749241 \
0 0 1 -36.74924,\
36.7492 36.74924\
1,36.749241 0 0 \
1 -36.74925,-36.\
7492 36.749241,3\
6.749241 0 0 1 3\
6.74925,-36.7493\
 36.749241,36.74\
9241 0 0 1 36.74\
924,36.7493 z m \
57.8846,18.2487 \
a 39.427414,39.4\
27414 0 0 1 -39.\
42742,39.4274 39\
.427414,39.42741\
4 0 0 1 -39.4274\
2,-39.4274 39.42\
7414,39.427414 0\
 0 1 39.42742,-3\
9.4274 39.427414\
,39.427414 0 0 1\
 39.42742,39.427\
4 z m -100.89344\
,-44.2206 a 39.4\
27414,39.427414 \
0 0 1 -39.42742,\
39.4274 39.42741\
4,39.427414 0 0 \
1 -39.42741,-39.\
4274 39.427414,3\
9.427414 0 0 1 3\
9.42741,-39.4274\
3 39.427414,39.4\
27414 0 0 1 39.4\
2742,39.42743 z\x22\
\x0a         inksca\
pe:connector-cur\
vature=\x220\x22 />\x0a  \
  </g>\x0a  </g>\x0a</\
svg>\x0a\
"

qt_resource_name = b"\
\x00\x04\
\x00\x077\xfe\
\x00m\
\x00a\x00i\x00n\
\x00\x06\
\x06\x8a\x9c\xb3\
\x00a\
\x00s\x00s\x00e\x00t\x00s\
\x00\x08\
\x05\xe2T\xa7\
\x00l\
\x00o\x00g\x00o\x00.\x00s\x00v\x00g\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x0e\x00\x02\x00\x00\x00\x01\x00\x00\x00\x03\
\x00\x00\x00 \x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x01, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x01, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
