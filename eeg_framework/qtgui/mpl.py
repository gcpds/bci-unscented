# import os
import time
import matplotlib
from cycler import cycler

import numpy as np

from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

matplotlib.use('Qt5Agg')
plt.style.use('dark_background')
q = matplotlib.cm.get_cmap('tab20')
matplotlib.rcParams['axes.prop_cycle'] = cycler(color=[q(m) for m in np.linspace(0, 1, 16)])
matplotlib.rcParams['figure.dpi'] = 60

from PySide2.QtCore import QThread, QTimer, QEventLoop, QRunnable, Slot, QThreadPool, QObject, Signal
# from PySide2.QtGui import QSi
import random

# SAMPLE_RATE = 1 / FPS
SECONDS = 30

import traceback

FPS = 5
PACK_SIZE = 1000 / FPS
# UPDATE = 1000 / 30
SUBSAMPLE = 1

MARK = 5

# SAMPLE_RATE = SAMPLE_RATE // SUBSAMPLE


# from openbci.acquisition import CytonRFDuino
# from openbci.database import load_sample_8ch_bin
from functools import wraps

# device = CytonRFDuino()
# device.start_collect()
# device.pack_data(milliseconds=PACK_SIZE)

import sys


# ----------------------------------------------------------------------
def set_fps(method):
    """"""
    global FPS, HZ
    HZ = []

    @wraps(method)
    def wrapped(self, *args, **kwargs):
        global FPS, HZ

        t0 = time.time()
        method(self, *args, **kwargs)
        t1 = time.time()

        HZ.append(1 / (t1 - t0))

        if len(HZ) > 100:
            HZ.pop(0)

        FPS = np.mean(HZ)

        if FPS > 10:
            FPS -= 10

        self.timer.interval = FPS

        print(f"{FPS:.2f} FPS | {HZ[-1]:.2f} Hz |   {self.y.shape[1]}  |  {int(SECONDS * FPS)}")

    return wrapped


class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        `tuple` (exctype, value, traceback.format_exc() )

    result
        `object` data returned from processing, anything

    progress
        `int` indicating % progress

    '''
    finished = Signal()
    error = Signal(tuple)
    result = Signal(object)
    progress = Signal(int)


class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        # self.kwargs['progress_callback'] = self.signals.progress

    @Slot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


########################################################################
class MatplotlibWidget(FigureCanvas):

    # ----------------------------------------------------------------------
    def __init__(self, threadpool=None):
        super().__init__(Figure())

        self.t0 = time.time()
        self.ax = self.figure.add_subplot(111)

        self.ax.grid(False)
        self.draw()

        self.ax_size = self.ax.bbox.width, self.ax.bbox.height
        self.ax_background = self.copy_from_bbox(self.ax.bbox)

        self.x = np.linspace(0, SECONDS, SECONDS * FPS)
        self.y = np.zeros((16, self.x.shape[0]))

        self.lines = []

        for i in range(16):
            self.lines.append(self.ax.plot(self.x, np.sin(self.x * 2), '-', animated=True, color=f'C{i}')[0])
        self.draw()

        self.start()

        # worker = Worker(self.start)  # Any other args, kwargs are passed to the run function
        # worker.signals.result.connect(self.print_output)
        # worker.signals.finished.connect(self.thread_complete)
        # worker.signals.progress.connect(self.progress_fn)

        # # Execute
        # threadpool.start(worker)

    # ----------------------------------------------------------------------
    def start(self):

        self.timer = self.new_timer(interval=1000 / FPS)
        self.timer.add_callback(self.timer_event)
        self.timer.start()

        # self.dataCollectionTimer = QTimer()
        # # self.dataCollectionTimer.moveToThread(self)
        # self.dataCollectionTimer.timeout.connect(self.timer_event)

        # self.dataCollectionTimer.start(1000 / FPS)
        # loop = QEventLoop()
        # loop.exec_()

    # ----------------------------------------------------------------------
    def timer_event(self):
        """"""
        current_size = self.ax.bbox.width, self.ax.bbox.height

        redraw = False

        if self.ax_size != current_size:

            self.ax_size = current_size
            self.ax.clear()
            self.ax.grid()
            self.draw()
            # self.draw()

            self.ax.set_ylim(0, 16)
            self.ax.set_xlim(-SECONDS, 0)
            self.ax_background = self.copy_from_bbox(self.ax.bbox)

            redraw = True

        self.restore_region(self.ax_background)

        # if not device.eeg_pack.qsize():
            # return False

        # print(device.eeg_pack.qsize())
        # sample, eeg, aux, footer, timestamp = device.eeg_pack.get()
        # eeg = eeg.T
        eeg = np.random.normal(size=(16, 100))

        if eeg[0] is None:
            return False

        for i, line in enumerate(self.lines):

            line = self.lines[i]
            data_p = self.y[i]
            data = 1 * eeg[i]

            if redraw:
                self.draw()

            points = int(SECONDS * FPS)

            if data_p.shape[0] > points:
                data_p = data_p[-points:]
                self.y = self.y[:, -points:]

            elif data_p.shape[0] < points:
                length = (points - data_p.shape[0])
                data_p = np.concatenate([np.zeros(length), data_p])
                self.y = np.insert(self.y, 0, np.zeros((length, len(self.lines))), axis=1)

            data_p = np.roll(data_p, -1)

            if self.t0 + MARK < time.time() and i == 0:
                data_p[-1] = 5
                self.t0 = time.time()
            else:
                data_p[-1] = np.mean(data)

            self.y[i] = data_p

            data_p = data_p - np.mean(data_p)
            data_p = i + 0.5 + (data_p / np.max(data_p)) * 0.8

            line.set_ydata(data_p)
            line.set_xdata(np.linspace(-SECONDS, 0, data_p.shape[0]))

            self.ax.draw_artist(line)
        self.blit(self.ax.bbox)
        return True

    def progress_fn(self, n):
        print("%d%% done" % n)

    def print_output(self, s):
        print(s)

    def thread_complete(self):
        print("THREAD COMPLETE!")
