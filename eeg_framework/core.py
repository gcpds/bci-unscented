# import os
# import sys
# from multiprocessing import Process, Pool
# from threading import Thread

from datetime import datetime, timedelta
import numpy as np

from PySide2.QtUiTools import QUiLoader
from PySide2 import QtGui, QtCore, QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2.QtCore import *

# from PySide2.Qtsc

from .qtgui.mpl import MatplotlibWidget
from .qtgui.icons import resource_rc
from .highlighter import PythonHighlighter


########################################################################
class EEGFramework(QtWidgets.QMainWindow):

    # ----------------------------------------------------------------------
    def __init__(self):
        super().__init__()

        self.main = QUiLoader().load('eeg_framework/qtgui/main.ui', self)
        self.main.pushButton_add_visualizarion.clicked.connect(self.add_subwindow)
        self.threadpool = QtCore.QThreadPool()

        self.main.actionEditor.triggered.connect(lambda evt: self.show_interface('Editor'))
        self.main.actionVisualization.triggered.connect(lambda evt: self.show_interface('Visualization'))
        self.main.actionStimulus_dashboard.triggered.connect(lambda evt: self.show_interface('Stimulus_dashboard'))

        # for i in range(6):
            # self.add_subwindow()

        for i in range(self.main.toolBar_environs.layout().count()):
            tool_button = self.main.toolBar_environs.layout().itemAt(i).widget()
            tool_button.setMaximumWidth(200)
            tool_button.setMinimumWidth(200)

        self.set_time_labels()
        self.set_editor()

    # ----------------------------------------------------------------------
    def add_subwindow(self):
        """"""

        sub = QMdiSubWindow()
        sub.setWindowFlag(QtCore.Qt.FramelessWindowHint)

        plot = MatplotlibWidget(self.threadpool)

        sub.setWidget(plot)
        self.main.mdiArea.addSubWindow(sub)
        sub.show()

        self.main.mdiArea.tileSubWindows()

    # ----------------------------------------------------------------------
    def show_interface(self, interface):
        """"""
        self.main.stackedWidget.setCurrentWidget(getattr(self.main, f"page{interface}"))
        for action in self.main.toolBar_environs.actions():
            action.setChecked(False)

        getattr(self.main, f"action{interface}").setChecked(True)

    # ----------------------------------------------------------------------
    def set_time_labels(self):
        """"""
        T = 15 * 8
        N = (self.main.horizontalLayout_time.layout().count() // 2) + 1
        curr = datetime.today() + timedelta(minutes=0)

        for i, t in zip(range(N), np.linspace(0, T, N)):
            label = getattr(self.main, f"label_time_{i}")
            curr = curr + timedelta(minutes=t)
            label.setText(curr.strftime('%H:%M'))

    # ----------------------------------------------------------------------
    def set_editor(self):
        """"""
        editor = self.main.textEdit

        editor.setStyleSheet("""

        QTextEdit {

        background-color: #000a12;
        color: white;
        height: 18px;
        font-weight: unset;
        font-family: 'mono';


        border: 1px solid #263238;
        border-radius: 4px;
        padding: 8px 16px ;
        height: 18px;


        }


        """)

        PythonHighlighter(editor.document())
        # font = QFont()
        # font.setFamily('mono')
        # font.setFixedPitch(True)
        # font.setPointSize(10)
        # editor.setFont(font)
        # # editor.setMarginsFont(font)

        infile = open('eeg_framework/core.py', 'r')
        editor.setPlainText(infile.read())
        infile.close()

        # editor

